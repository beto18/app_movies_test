package com.test.moviesapp

import android.app.Application
import com.test.moviesapp.di.commonModule
import com.test.moviesapp.di.networkModule
import com.test.moviesapp.di.repositoryModule
import com.test.moviesapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App :Application() {

    override fun onCreate() {
        super.onCreate()
        initSettings()
    }

    private fun initSettings(){
        startKoin {
            androidContext(this@App)
            modules(listOf( commonModule,networkModule, repositoryModule,viewModelModule))
        }
    }
}