package com.test.moviesapp.base

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel(){
    abstract fun onReady(fragment: Fragment)
}