package com.test.moviesapp.model.movie

data class Media(
    val code: String,
    val resource: String,
    val type: String
)