package com.test.moviesapp.model.movie

data class Cast(
    val label: String,
    val value: List<String>
)