package com.test.moviesapp.model.movie

import com.google.gson.annotations.SerializedName

data class Movy(
    val cast: List<Cast>,
    val categories: List<String>,
    val cinemas: List<Int>,
    val code: String,
    val distributor: String,
    val genre: String,
    val id: Int,
    val length: String,
    val media: List<Media>,
    val name: String,
    @SerializedName("original_name")
    val originalName: String,
    val position: Int,
    val rating: String,
    @SerializedName("release_date")
    val releaseDate: String,
    val synopsis: String,
    var urlImage:String
)