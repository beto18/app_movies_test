package com.test.moviesapp.model.login

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName(".expires")
    val expires: String,
    @SerializedName(".issued")
    val issued: String,
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("as:client_id")
    val clientId: String,
    @SerializedName("country_code")
    val countryCode: String,
    @SerializedName("expires_in")
    val expiresIn: Int,
    @SerializedName("refresh_token")
    val refreshToken: String,
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("username")
    val userName: String
)