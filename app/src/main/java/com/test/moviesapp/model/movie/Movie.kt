package com.test.moviesapp.model.movie

data class Movie(
    val movies: List<Movy>,
    val routes: List<Route>
)