package com.test.moviesapp.model.perfil

data class Perfil(
    val card_number: String,
    val email: String,
    val first_name: String,
    val last_name: String,
    val phone_number: String,
    val profile_picture: String
)