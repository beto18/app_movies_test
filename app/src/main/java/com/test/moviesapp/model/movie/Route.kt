package com.test.moviesapp.model.movie

data class Route(
    val code: String,
    val sizes: Sizes
)