package com.test.moviesapp.model.login

import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import com.google.gson.Gson

class SessionBean(
    @Transient
    private val sharedPrefs: SharedPreferences
) {
    @Transient
    val TAG = SessionBean::class.java.simpleName

    var loginData: LoginResponse? = null


    init {
        recovery()
    }

    private fun recovery(){
        val jsonSession = sharedPrefs.getString(TAG, "")
        val sessionBean = Gson().fromJson(jsonSession!!, SessionBean::class.java)
        putData(sessionBean)
    }

    private fun putData(session: SessionBean?= null){
        Log.d("SessionBean","putData")
        if(session != null){
            loginData = session.loginData
        }else{
            loginData = null

        }
    }

    fun save(){
        val jsonSession = Gson().toJson(this@SessionBean)
        sharedPrefs.edit{
            putString(TAG,jsonSession)
        }
    }

    fun clear() {
        sharedPrefs.edit{
            clear()
        }
        putData()
        save()
    }
}