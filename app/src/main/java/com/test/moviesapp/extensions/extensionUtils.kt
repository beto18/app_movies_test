package com.test.moviesapp.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Activity.showToast(message:String){
    Toast.makeText(this,message, Toast.LENGTH_LONG).show()
}

fun Fragment.showToast(message:String){
    Toast.makeText(activity,message, Toast.LENGTH_LONG).show()
}

inline fun <reified T :Activity> Context.startActivity(){
    val intent = Intent(this, T::class.java)
    startActivity(intent)


}