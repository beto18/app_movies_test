package com.test.moviesapp.di

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.test.moviesapp.model.login.SessionBean
import org.koin.dsl.module

val commonModule = module {

    /***
     * Instancia de Session Bean
     */
    //SessionBean
    single(createdAtStart = true) { SessionBean(get()) }

    /***
     * Instancia de shared preferens
     */
    //Shared Preferences
    single(createdAtStart = true) { provideSharedPrefs(get()) }
}

private fun provideSharedPrefs(application: Application): SharedPreferences {
    return PreferenceManager.getDefaultSharedPreferences(application)
}