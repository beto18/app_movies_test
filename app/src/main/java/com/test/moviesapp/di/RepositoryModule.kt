package com.test.moviesapp.di

import com.test.moviesapp.domain.repository.BillboardRepository
import com.test.moviesapp.domain.repository.BillboardRepositoryImpl
import com.test.moviesapp.domain.repository.LoginRepository
import com.test.moviesapp.domain.repository.LoginRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {

 single<LoginRepository>(createdAtStart = true) {
  LoginRepositoryImpl(get(), get())
 }

 single<BillboardRepository>(createdAtStart = true) {
  BillboardRepositoryImpl(get())

 }
}