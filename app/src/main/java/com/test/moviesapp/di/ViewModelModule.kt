package com.test.moviesapp.di

import com.test.moviesapp.ui.billboard.BillboardViewModel
import com.test.moviesapp.ui.detailmovie.DetailMovieViewModel
import com.test.moviesapp.ui.login.LoginViewModel
import com.test.moviesapp.ui.profile.ProfileViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { BillboardViewModel(get()) }
    viewModel { DetailMovieViewModel() }
    viewModel { LoginViewModel(get()) }
    viewModel { ProfileViewModel(get()) }

}