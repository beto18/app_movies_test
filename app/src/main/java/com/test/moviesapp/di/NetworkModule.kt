package com.test.moviesapp.di

import com.test.moviesapp.BuildConfig
import com.test.moviesapp.domain.api.ApiBillboards
import com.test.moviesapp.domain.api.ApiLogin
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { provideOkHttpClient() }
    single { providerRetrofitSource<ApiLogin>( okHttpClient = get()) }
    single { providerRetrofitSource<ApiBillboards>(okHttpClient =  get()) }
}


private fun provideOkHttpClient(): OkHttpClient {
    val interceptorLogger = HttpLoggingInterceptor()
    interceptorLogger.level = HttpLoggingInterceptor.Level.BODY

    return OkHttpClient.Builder()
        .connectTimeout(BuildConfig.NETWORK_TIME_OUT, TimeUnit.SECONDS)
        .writeTimeout(BuildConfig.NETWORK_TIME_OUT, TimeUnit.SECONDS)
        .readTimeout(BuildConfig.NETWORK_TIME_OUT, TimeUnit.SECONDS)
        .addInterceptor(interceptorLogger)
        .build()
}

private inline  fun<reified T> providerRetrofitSource(urlBase:String? = BuildConfig.BASE_URL, okHttpClient: OkHttpClient):T{

    val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .baseUrl(urlBase)
        .build()

    return retrofit.create(T::class.java)
}