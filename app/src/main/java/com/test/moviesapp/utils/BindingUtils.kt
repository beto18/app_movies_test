package com.test.moviesapp.utils

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.squareup.picasso.Picasso
import com.test.moviesapp.R

class BindingUtils {

    companion object {
        /**
         * Binding adaoter xml
         * Muestra la imagen url
         */
        @JvmStatic
        @BindingAdapter("app:url")
        fun setImageGlide(container: AppCompatImageView, url: String?) {
            Glide.with(container.context)
                .load(url ?: R.drawable.not_found_image)
                .placeholder(R.drawable.image_loader)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.not_found_image)
                .into(container);

        }

        @JvmStatic
        @BindingAdapter("app:urlImage")
        fun setImagePicasso(container: AppCompatImageView, url: String?) {
            Picasso.get()
                .load( url)
                .placeholder(R.drawable.image_loader)
                .error(R.drawable.not_found_image)
                .into(container);
        }
    }
}