package com.test.moviesapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPagerFragmentAdapter(
    fragmentActivity: FragmentActivity,
    var listFragments: List<Fragment>
) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int {
        return listFragments.size
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return listFragments[0]
            1 -> return listFragments[1]
            2 -> return listFragments[2]
        }
        return listFragments[0]
    }


}