package com.test.moviesapp.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.moviesapp.databinding.ItemMovieLayoutBinding
import com.test.moviesapp.model.movie.Movy

class MovieAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items: ArrayList<Movy> = arrayListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemMovieLayoutBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(items[position])
    }

    override fun getItemCount() = items.size

    class ViewHolder(
        val binding: ItemMovieLayoutBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Movy) {
            binding.data = item
            binding.executePendingBindings()
        }



    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(data: List<Movy>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

}