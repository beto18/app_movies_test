package com.test.moviesapp.ui.profile

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewModelScope
import com.test.moviesapp.base.BaseViewModel
import com.test.moviesapp.domain.repository.LoginRepository
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ProfileViewModel(val loginRepository: LoginRepository)  : BaseViewModel(){

    val model by lazy { ProfileModelView() }
    override fun onReady(fragment: Fragment) {
        getProile()
    }


    fun getProile(){
        viewModelScope.launch {
            loginRepository.getPerfil()
                .catch {
                    Log.d("profile", it.message?: "Error")
                }.collect {
                    model.correo.set(it.email)
                    model.user.set("${it.first_name}  ${it.last_name}")
                    model.tarjet.set(it.card_number)
                }
        }
    }
}