package com.test.moviesapp.ui.profile

import androidx.databinding.ObservableField

class ProfileModelView {

    val correo = ObservableField<String>()
    val tarjet = ObservableField<String>()
    val user = ObservableField<String>()
    val imagen = ObservableField<String>()


}