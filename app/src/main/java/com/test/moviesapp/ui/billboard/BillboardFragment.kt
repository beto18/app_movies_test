package com.test.moviesapp.ui.billboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.test.moviesapp.base.BaseFragment
import com.test.moviesapp.databinding.FragmentBillboardBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class BillboardFragment : BaseFragment<BillboardViewModel>() {

    override val vm: BillboardViewModel by viewModel{ parametersOf(this) }

    private lateinit var binding:FragmentBillboardBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBillboardBinding.inflate(layoutInflater, container, false)
        // Inflate the layout for this fragment
        binding.vm = vm
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance() = BillboardFragment()
    }
}