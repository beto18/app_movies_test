package com.test.moviesapp.ui.billboard

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewModelScope
import com.test.moviesapp.adapter.MovieAdapter
import com.test.moviesapp.base.BaseViewModel
import com.test.moviesapp.domain.repository.BillboardRepository
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class BillboardViewModel(val billboardRepository: BillboardRepository) : BaseViewModel() {

    val adapter by lazy { MovieAdapter() }

    override fun onReady(fragment: Fragment) {
        getMovies()
    }


    fun getMovies(){
        viewModelScope.launch {
            billboardRepository.getMovies()
                .catch {
                    Log.d("Movies", it.message!!)
                }
                .collect {
                    adapter.setData(it!!.movies)
                }
        }

    }
}