package com.test.moviesapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.test.moviesapp.R
import com.test.moviesapp.adapter.ViewPagerFragmentAdapter
import com.test.moviesapp.databinding.ActivityMainBinding
import com.test.moviesapp.ui.billboard.BillboardFragment
import com.test.moviesapp.ui.profile.ProfileFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding:  ActivityMainBinding
    private val fragments = arrayListOf(ProfileFragment.newInstance(), BillboardFragment.newInstance())
    private val titles = arrayOf(PROFILE, BILLBOARD, COMPLEJO)

    companion object {
        val PROFILE = "Perfíl"
        val BILLBOARD = "Cartelera"
        val COMPLEJO = "Complejo"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.homeFVPager.adapter = ViewPagerFragmentAdapter(this,fragments)

        init()
    }


    private fun init() {
        with(binding) {
            homeFVPager.let { v->
                homeFTabLayout.let { tabl ->
                    TabLayoutMediator(tabl, v) {
                            tab: TabLayout.Tab, position: Int ->
                        tab.text = titles[position]
                    }.attach()
                }
            }


        }
    }
}