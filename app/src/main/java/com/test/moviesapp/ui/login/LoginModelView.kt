package com.test.moviesapp.ui.login

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField

class LoginModelView {

    var usuario = ObservableField<String>()
    var password = ObservableField<String>()
    var isShowing = ObservableBoolean(false)
}