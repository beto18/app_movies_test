package com.test.moviesapp.ui.login

import android.os.Bundle
import com.test.moviesapp.base.BaseActivity
import com.test.moviesapp.databinding.ActivityLoginBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class LoginActivity : BaseActivity() {

    val vm: LoginViewModel by viewModel { parametersOf(this) }
    override fun onCreate(savedInstanceState: Bundle?) {
        val binding = ActivityLoginBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        binding.vm = vm
        setContentView(binding.root)
    }

    override fun initSettings() {
        vm.initSettings(this)
    }
}