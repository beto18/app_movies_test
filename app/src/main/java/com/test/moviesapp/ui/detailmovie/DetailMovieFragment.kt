package com.test.moviesapp.ui.detailmovie

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.test.moviesapp.R
import com.test.moviesapp.base.BaseFragment
import com.test.moviesapp.databinding.FragmentDetailMovieBinding
import com.test.moviesapp.databinding.FragmentProfileBinding
import com.test.moviesapp.ui.billboard.BillboardViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class DetailMovieFragment : BaseFragment<DetailMovieViewModel>() {

    override val vm: DetailMovieViewModel by viewModel{ parametersOf(this) }

    private lateinit var binding: FragmentDetailMovieBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDetailMovieBinding.inflate(layoutInflater, container, false)
        binding.vm = vm
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            DetailMovieFragment()
    }
}