package com.test.moviesapp.ui.login

import android.annotation.SuppressLint
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.moviesapp.domain.repository.LoginRepository
import com.test.moviesapp.extensions.showToast
import com.test.moviesapp.extensions.startActivity
import com.test.moviesapp.ui.MainActivity
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class LoginViewModel(val loginRepository: LoginRepository): ViewModel() {

    val model by lazy { LoginModelView() }
    @SuppressLint("StaticFieldLeak")
    private var activity:AppCompatActivity? = null


    fun initSettings(activity: AppCompatActivity){
        this.activity = activity
    }


    fun callLogin(){
        if (model.usuario.get() != null){
            if (model.password.get() != null){
                viewModelScope.launch {
                    loginRepository.callLogin(model.usuario.get()!!, model.password.get()!!)
                        .onStart {
                            model.isShowing.set(true)
                        }
                        .onCompletion {
                            model.isShowing.set(false)
                        }
                        .catch {
                            activity!!.showToast(it.message!!)
                            Log.d("Login", it.message!!)

                        }.collect {
                            activity!!.startActivity<MainActivity>()
                            activity!!.finish()
                        }
                }
            } else{
                activity!!.showToast("Favor de ingresar su contraseña")

            }
        }else{
            activity!!.showToast("Favor de ingresar un usuario")
        }


    }

}