package com.test.moviesapp.domain.api

import com.test.moviesapp.model.movie.Movie
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


const val CINEMAS: String = "61"

interface ApiBillboards {


    @Headers("api_key: stage_HNYh3RaK_Test")
    @GET("/v2/movies")
    suspend fun getMovies(
        @Query("country_code") country: String = COUNTRY,
        @Query("cinemas") cinemas: String = CINEMAS
    ): Movie
}