package com.test.moviesapp.domain.repository

import android.accounts.NetworkErrorException
import com.test.moviesapp.BuildConfig
import com.test.moviesapp.domain.api.ApiBillboards
import com.test.moviesapp.model.movie.Movie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.retryWhen
import java.util.concurrent.TimeUnit

interface BillboardRepository{

    fun getMovies():Flow<Movie?>
}

class BillboardRepositoryImpl(val api:ApiBillboards): BillboardRepository{
    override fun getMovies(): Flow<Movie?>  = flow{
        val result: Movie?
        try {
            result = api.getMovies()
            for( movie in result.movies){
                val poster = result.routes.find { media ->  media.code == "poster"}
                val base = poster!!.sizes.medium
                val path = movie.media.find { media ->  media.code == "poster" }?.resource ?: ""
                val url = "${base}${path} "
                movie.urlImage = url
            }

        }catch (e:Exception){
            throw Exception(e.message)
        }

        emit(result)

    }.retryWhen { cause, attempt ->
        cause !is NetworkErrorException && attempt < BuildConfig.RETRY_CALL_SERVICES
    }.flowOn(Dispatchers.IO)


}