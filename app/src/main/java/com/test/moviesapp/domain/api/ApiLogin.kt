package com.test.moviesapp.domain.api

import com.google.gson.JsonObject
import com.test.moviesapp.model.perfil.Perfil
import retrofit2.http.*


const val COUNTRY: String  = "MX"
const val TYPE: String  = "password"
const val USER: String  = "pruebas_beto_ia%40yahoo.com"
const val PASSWORD: String  = "Pruebas01"
const val CLIENT_ID: String  = "IATestCandidate"
const val CLIENT_SECRET: String  = "c840457e777b4fee9b510fbcd4985b68"

interface ApiLogin {



    @FormUrlEncoded
    @Headers("Content-Type: application/x-www-form-urlencoded",
        "api_key:stage_HNYh3RaK_Test",
        "Cache-Control:no-cache")
    @POST("/v2/oauth/token")
    suspend fun login(
        @Field("country_code")conutry:String = COUNTRY,
        @Field("username") user:String,
        @Field("password") password:String,
        @Field("grant_type") type:String = TYPE,
        @Field("client_id") client:String = CLIENT_ID,
        @Field("client_secret")secret:String = CLIENT_SECRET):JsonObject

    @Headers("api_key:stage_HNYh3RaK_Test")
    @GET("/v1/members/profile")
    suspend fun getPerfil( @Header("Authorization") token:String,
                           @Query("country_code") conutry: String = COUNTRY):Perfil
}