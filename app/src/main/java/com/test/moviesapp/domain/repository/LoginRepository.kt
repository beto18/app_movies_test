package com.test.moviesapp.domain.repository

import android.media.metrics.PlaybackErrorEvent
import com.google.gson.Gson
import com.test.moviesapp.domain.api.ApiLogin
import com.test.moviesapp.model.login.LoginResponse
import com.test.moviesapp.model.login.SessionBean
import com.test.moviesapp.model.perfil.Perfil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.retryWhen

interface LoginRepository {

    fun callLogin(user: String, password: String): Flow<Boolean>
    fun getPerfil(): Flow<Perfil>
}

class LoginRepositoryImpl(
    val api: ApiLogin,
    val sessionBean: SessionBean
) : LoginRepository {
    override fun callLogin(user: String, password: String): Flow<Boolean> = flow {
        val result = api.login(user = user, password = password)
        if (result.get("error_description") == null) {
            val loginResponse = Gson().fromJson(result.toString(), LoginResponse::class.java)
            sessionBean.loginData = loginResponse
        } else {
            throw Exception(result.get("error_description").toString())

        }


        emit(true)
    }.flowOn(Dispatchers.IO)

    override fun getPerfil(): Flow<Perfil> = flow<Perfil> {
        val result: Perfil

        try {
            result = api.getPerfil(sessionBean.loginData!!.accessToken)
        } catch (e: Exception) {
            throw Exception(e.message)
        }

        emit(result)
    }.flowOn(Dispatchers.IO)

}